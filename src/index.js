var express = require('express');
var url = require('url');
import cors from 'cors';

var lesson2 = require('./lessons/lesson2');
var lesson3 = require('./lessons/lesson3');

const app = express();
app.use(cors());

app.get('/', function (req, res) {
    res.send('Skill branch.');
});

app.get('/2A', function (req, res) {
    res.send(lesson2.exerciseA(req.query.a, req.query.b));
});

app.get('/2B', function (req, res) {
    res.send(lesson2.exerciseB(req.query.fullname));
});

app.get('/2C', function (req, res) {
    res.send(lesson2.exerciseC(req.query.username));
});

app.get('/2D', function (req, res) {
    res.send(lesson2.exerciseD(req.query.color));
});

app.get('/3B/*', function (req, res) {
    lesson3.requestHandler(req, res);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});