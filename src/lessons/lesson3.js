require('es6-promise').polyfill();
require('isomorphic-fetch');

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
    .then(async (res) => {
        pc = await res.json();
    })
    .catch(err => {
        console.log('Чтото пошло не так:', err);
    });

function execute(params = '') {
    var ret = pc;

    params.forEach((v) => {
        if (v && ret && (typeof ret != 'string' && v in ret)) {
            if (ret instanceof Array && (isNaN(+v) || typeof (+v) != 'number')) {
                ret = false;
            } else {
                ret = ret[v];
            }
        } else {
            if (!v) {
                return;
            }
            ret = false;
        }
    });

    return ret;
}

function volumes() {
    var hddFreeSpace = {};

    pc.hdd.forEach(v => {
        if (hddFreeSpace[v.volume]) {
            let size = parseInt(hddFreeSpace[v.volume]);
            size += v.size;
            hddFreeSpace[v.volume] = size + 'B'
        } else {
            hddFreeSpace[v.volume] = v.size + 'B';
        }
    });

    return hddFreeSpace;
}

function requestHandler(req, res) {
    let reqStr = req.path.slice(4);
    if (reqStr == 'volumes') {
        res.json(volumes());
        return;
    }

    let ret = execute(reqStr.split('/'));

    if (ret === false) {
        res.status(404).send('Not Found');
        return;
    }

    res.json(ret);
}

exports.requestHandler = requestHandler;