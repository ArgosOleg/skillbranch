const convert = require('color-convert');

const regUserName = /(https?:|\@)?(\/\/)?([.a-z0-9-]+\.[-0-1a-z]+\/)?(@)?([_a-z0-9\.]+)(.*)/i;
const regColorHex = /^(#)?([a-f0-9]{3}|[a-f0-9]{6})$/i;
const regColorRGB = /(^rgb\()((\s*[0-9]{1,3}\s*,?){3})(\))/i;
const regColorHSL = /(^hsl\()((\s*[0-9]{1,3}%?\s*,?){3})(\))/i;

module.exports = {
    exerciseA : function (a = 0, b = 0) {
        return (+a + +b).toString();
    },

    exerciseB : function (fullName) {
        fullName = fullName.toString().trim();
        if (!fullName || fullName.match(/[0-9]|_|\//g) !== null) {
            return 'Invalid fullname';
        }
        fullName = fullName.split(/\s{1,}/);

        function capitalize(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        let shortName = '';

        switch(fullName.length) {
            case 3:
                shortName =
                    capitalize(fullName[2].toLowerCase()) + ' ' +
                    fullName[0].toUpperCase().charAt(0) + '. ' +
                    fullName[1].toUpperCase().charAt(0) + '.' ;
                break;
            case 2:
                shortName =
                    capitalize(fullName[1].toLowerCase()) + ' ' +
                    fullName[0].toUpperCase().charAt(0) + '.';
                break;
            case 1:
                shortName = capitalize(fullName[0].toLowerCase());
                break;
            default:
                return 'Invalid fullname';
        }
        return shortName;
    },

    exerciseC : function (username) {
        username = username.toString().trim();
        if (!username) {
            return 'Invalid fullname';
        }

        username = username.match(regUserName);

        if (!username[5]) {
            return 'Invalid fullname';
        }

        return '@'+username[5];
    },

    /**
     * @param str; color string
     * @returns {*}
     */
    exerciseD : function (str = '') {
        str = str.toString().trim().toLowerCase().replace(/%20/g, '');

        let color;

        if ((color = parseRGBToHex(str)) === false) {

            if ((color = parseHSLToHex(str)) === false) {

                if ((color = parseHex(str)) === false) {
                    return 'Invalid color';
                }
            }
        }
        return '#' + color.toLowerCase();


    }
};

/**
 * @param str;
 *
 * @return boolean | string;
 * check if param str contain valid hex color and return them if true,
 * otherwise return false, if str don't contain valid hex color code
 */
function parseHex(str) {

    let color = str.match(regColorHex);

    if (color == null || !(2 in color)) {
        return false;
    }
    color = color[2];

    if (color.length == 3) {
        color = [...color].map((char) => { return char + char; }).join('');
    }

    return color;
}

/**
 * @param str
 *
 * @return boolean | string;
 * return false if param str don't contain rgb color
 * return hex color code if str valid rgb string
 */
function parseRGBToHex(str) {

    let color = str.match(regColorRGB);

    if (color == null || !(2 in color)) {
        return false;
    }

    color = color[2]
        .split(/([,\s])/)
        .filter((val) => { return val != '' && val != ' ' && val != ','})
        .filter((num) => {
            num = parseInt(num);
            return num < 256 && num >= 0
        });

    if (color.length !== 3) {
        return false;
    }

    return convert.rgb.hex(color);
}

/**
 * @param str
 *
 * @return boolean | string;
 * return false if param str don't contain hsl color
 * return hex color code if str valid hsl string
 */
function parseHSLToHex(str) {

    let color = str.match(regColorHSL);

    if (color == null || !(2 in color)) {
        return false;
    }

    color = color[2]
        .split(/([,\s])/)
        .filter((val) => { return val != '' && val != ' ' && val != ','})
        .filter((num) => {
            if (num.search('%') == -1) {
                return true;
            }
            num = parseInt(num);
            return num <= 100 && num >= 0;
        });

    if (color.length !== 3) {
        return false;
    }

    if (color[1].search('%') == -1 || color[2].search('%') == -1) {
        return false;
    }

    color = color.map((num) => {
        return parseInt(num);
    });

    return convert.hsl.hex(color);
}